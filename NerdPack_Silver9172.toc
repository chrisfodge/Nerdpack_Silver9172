## Title: |r[|cff0070deNerdPack|r] - Silver9172
## Notes: NerdPack Combat Routines
## Interface: 60200
## Dependencies: NerdPack
## Author: Silver9172

core.lua

Paladin\Holy\rotation.lua
Paladin\Ret\rotation.lua
Paladin\Prot\rotation.lua
Paladin\Prot\events.lua

Warrior\Prot\rotation.lua
Warrior\Fury\rotation.lua
