-- ProbablyEngine Rotation Packager

local rotation = {
	-- Normal Single Target
	{{
		{ 'Berserker Rage', {
			'!player.buff(Enrage)',
			'!spell.cooldown(Bloodthirst) = 0'
		}},
		{ 'Execute', {
			'player.buff(Sudden Death)'
		}},
		{ 'Wild Strike', {
			'player.rage > 90'
		}},
		{ 'Raging Blow', {
			'player.buff(Raging Blow!).count = 2'
		}},
		{ 'Bloodthirst', {
			'!player.buff(Enrage)',
			'talent(3,3)'
		}},
		{ 'Bloodthirst', {
			'!player.buff(Enrage)',
			'!talent(3,3)',
			'player.rage < 80',
		}},
		{ 'Wild Strike', {
			'player.buff(Bloodsurge)',
		}},
		{ 'Storm Bolt'},
		{ 'Dragon Roar'},
		{ 'Raging Blow'},
		{ 'Wild Strike', {
			'player.buff(Enrage)'
		}},
		{ 'Bloodthirst', {
			'talent(3,3)'
		}},
	}, 'target.health > 20'},
	
	-- Execute
	{{
		{ 'Berserker Rage', {
			'!player.buff(Enrage)',
			'!spell.cooldown(Bloodthirst) = 0'
		}},
		{ 'Execute', {
			'player.rage > 110'
		}},
		{ 'Bloodthirst', {
			'!player.buff(Enrage)',
			'talent(3,3)'
		}},
		{ 'Bloodthirst', {
			'!player.buff(Enrage)',
			'!talent(3,3)',
			'player.rage < 90',
		}},
		{ 'Storm Bolt'},
		{ 'Dragon Roar'},
		{ 'Execute', {
			'player.buff(Enrage)'
		}},
		{ 'Wild Strike', {
			'player.buff(Bloodsurge)',
		}},
		{ 'Raging Blow'},
	}, 'target.health <= 20'}
}


NeP.Engine.registerRotation(72, 'Fury Warrior - Silver9172', {
	{ rotation},
},

{ -- Out of Combat
	{ '6673', '!player.buff'}
})


