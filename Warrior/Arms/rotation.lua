local target = {
	{{ -- Targeting
		{ '/targetenemy [noexists]', { -- Target an enemy
			'!target.exists',
		}},
		
		{ '/focus [@targettarget]',{ -- Set focus to current targets target
			'target.enemy',
			'target(target).friend' ,
		}},
	}, 'toggle.AutoTarget'},
}

local survival = {
	{ '118038', {
		'player.heath <= 30',
	}},
	{ '97462', {
		'player.health <= 40'
	}},
	{ '103840', {
		'player.health <= 60',
		'target.range <= 8'
	}},
}

local cooldowns = {
	{ '1719'},
	{ '12292'},
	{ '20572'},
}

local moving = {
	
}

local singleTarget = {
	{ '1715', {
		'!target.debuff',
		'target.level == 100',
	}},
	
	{{
		{ cooldowns, 'modifier.cooldowns'},
		
		-- Execute
		{ '163201', {
			'player.buff(52437)'
		}},
		
		{ '163201', {
			'target.health <= 20'
		}},
		
		-- Mortal Strike
		{ '12294', {
			'target.health > 20'
		}},
		
		-- Seigebreaker
		{ '176289', {
			'target.health > 20'
		}},
		{ '176289', {
			'target.health <= 20',
			'player.rage <= 40'
		}},
		
		-- Storm Bolt/Dragon roar
		{ '107570', {
			'target.health > 20',
		}},
		{ '107570', {
			'target.health >= 20',
			'player.rage <= 40'
		}},
		
		-- Thunder Clap
		{ '6343', {
			'target.range <= 9',
			'target.health > 20'
		}},
		
		-- Whirlwind
		{ '1680', {
			'target.health > 20'
		}},
		
		-- rend
		{ '772', 'target.debuff.duration <= 4'},
	}, 'target.debuff(167105)'},
	
	{{		
		-- Execute
		{ '163201', {
			'player.buff(52437)'
		}},
		
		-- rend
		{ '772', 'target.debuff.duration <= 4'},
		
		-- Smash goes here!!
		{ '167105', {
			'player.rage >= 60',
			'player.spell(176289).cooldown <= 3',
		}},
		{ '167105', {
			'player.rage >= 60',
			'player.spell(176289).cooldown >= 20',
		}},
		
		{ '163201', {
			'target.health <= 20',
			'player.rage >= 80',
		}},
		
		-- Mortal Strike
		{ '12294', {
			'target.health > 20',
			'player.spell(167105).cooldown >= 6'
		}},
		{ '12294', {
			'target.health > 20',
			'player.rage >= 110'
		}},
		
		-- Storm Bolt/Dragon roar
		{ '107570', {
			'player.rage <= 89'
		}},
		
		-- Thunder Clap
		{ '6343', {
			'target.range <= 9',
			'target.health > 20'
		}},
		
		-- Whirlwind
		{ '1680',{
			'player.rage >= 40',
			'player.spell(167105).cooldown >= 6',
			'target.health > 20'
		}},
		{ '1680',{
			'player.rage >= 110',
			'target.health > 20'
		}},
	}, '!target.debuff(167105)'},

}

NeP.Engine.registerRotation(71, 'Arms Warrior', {
	{ survival},
	{ singleTarget},

},

{ -- Out of Combat

},

init)
  
 
