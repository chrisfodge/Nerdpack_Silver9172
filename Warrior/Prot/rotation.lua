-- Protection Warrior
local dynEval = NePCR.dynEval
local fetchKey = NeP.Interface.fetchKey

-- Toggle AoE with a in game macro
SLASH_HELLOWORLD1, SLASH_HELLOWORLD2 = '/hiw', '/hellow'; -- 3.
function SlashCmdList.HELLOWORLD(msg, editbox) -- 4.
 local multiTargetOn = NeP.Config.Read('bStates_AoE')
 	if multiTargetOn then
 		print('Multitarget is on, turning it off')
 		NeP.Config.Write('bStates_AoE', false)
 	else
 		print('Multitarget is off, turning it on')
 		NeP.Config.Write('bStates_AoE', true)
	end
end 

local config = {
	key = 'ProtWarrior',
	profiles = true,
	title = 'Config',
	subtitle = 'Prot Warrior Settings',
	color = NeP.Core.classColor('player'),
	width = 250,
	height = 500,
	config = {

		-- General
		{type = 'header', text = 'General Settings:', align = 'center'},
		
	}
}

NeP.Interface.buildGUI(config)

local init = function()
	NeP.Interface.CreateSetting('Class Settings', function() NeP.Interface.ShowGUI('ProtWarrior') end)
	NeP.Interface.CreateToggle(
		'AutoTarget', 
		'Interface\\Icons\\ability_hunter_snipershot', 
		'Auto Target and Focus',
		'Target boss and focus currently active Tank')
	NeP.Interface.CreateToggle(
		'block', 
		'Interface\\ICONS\\ability_defend', 
		'Shield Block', 
		'Use shield block in defenive rotation!')
end

local target = {
	{{ -- Targeting
		{ '/targetenemy [noexists]', { -- Target an enemy
			'!target.exists',
		}},
	}, 'toggle.AutoTarget'},
}

local keybinds = {

}

local survival = {
	{ "202168", { -- Impending Victory
		"player.health <= 85"
	}},
	
	{ "871", { -- Shield Wall
		"player.health <= 35",
		"modifier.cooldowns",
	}},
	{ "12975", { -- Last Stand
		"player.health <= 20",
		"modifier.cooldowns",
	}},
	{ "1160", { -- Demo roar
		"player.health <= 90",
		"modifier.cooldowns",
	}},
	
	{ "2565", { -- Shield Block
		'player.spell.charges >= 2',
		'player.rage > 10'
	}},
	{ "Ignore Pain", { -- Ignore Pain
		"player.rage >= 40",
		"!player.buff",
	}},
	
	{ 'Ignore Pain', {
		'player.buff(Vengeance: Ignore Pain)',
		'!player.buff'
	}},
	
	{ 'Focused Rage', {
		'player.buff(Vengeance: Focused Rage)'
	}},
	
	{ '/use Purified Shard of the Third Moon', {
	
	}}, 
}

local cooldowns = {
	{ '1719', {
		'target.range <= 8'
	}},
	{ '107574', {
		'target.range <= 8'
	}},
}

local rotation = {
	{ "57755", { -- Heroic Throw
		"target.range >= 9",
	}}, 
	
	{ '46968', { -- Shockwave
		'target.range <= 8'
	}}, 
	
	{ '6343', {
		'player.area(16).enemies >= 2'
	}},
	
	{{
		{ '6572'}, -- revenge
		{ '6343'}, -- Thunderclap
		{ "23922"}, -- Shield Slam
		{ "20243"}, -- Devistate
	}, 'target.area(8).enemies >= 2'},
	
	{{
		{ "23922"}, -- Shield Slam
		{ "6572"}, -- Revenge
		{ "107570"}, -- Storm Bolt
		{ "20243"}, -- Devistate
	}},
}

NeP.Engine.registerRotation(73, 'Prot Warrior - 7.0.3', {
	{ keybinds},
	{ survival},
	{ cooldowns, 'modifier.cooldowns'},
	{ rotation, 'target.enemy'},
},

{ -- Out of Combat	
	{ keybinds},
},

init)


