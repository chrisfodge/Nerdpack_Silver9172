-- NeP Prot Pally Rotation
local dynEval = NePCR.dynEval
local fetchKey = NeP.Interface.fetchKey

--[[ 
To Do List


- Blessing of Sac Support
	- On focus when certain abilities
	- On raid when certain abilities

- Support for all talents
- Blessing of Freedom Support
- Cleanse Toxin Support
	- Player > Focus > Raid
- Bastion of Light Support
	- When no stacks and long recharge
- Auto remove BoP/Divine Shield
- Hand of the Protector Support
- AOE Rotation	
- Option to keep SotR at X charges 
]]

-- Toggle AoE with a in game macro
SLASH_HELLOWORLD1, SLASH_HELLOWORLD2 = '/hiw', '/hellow'; -- 3.
function SlashCmdList.HELLOWORLD(msg, editbox) -- 4.
 local multiTargetOn = NeP.Config.Read('bStates_AoE')
 	if multiTargetOn then
 		print('Multitarget is on, turning it off')
 		NeP.Config.Write('bStates_AoE', false)
 	else
 		print('Multitarget is off, turning it on')
 		NeP.Config.Write('bStates_AoE', true)
	end
end 

local config = {
	key = 'ProtPallyRotation',
	profiles = true,
	title = 'Prot Pally',
	subtitle = 'Settings',
	color = NeP.Core.classColor('player'),
	width = 200,
	height = 500,
	config = {
		-- General Settings
		{type = 'header', text = 'General Settings:', align = 'center'},
			{type = 'checkbox', text = 'Target', key = 'Target', default = true, desc = 'Auto target new target when old one dies.'},
			{type = 'checkbox', text = 'WoD Ring', key = 'ring', default = true, desc = 'Auto use ring as DPS CD.'},
			
		-- Cooldown Settings
		-- {type = 'spacer'},{type = 'rule'},
		{type = 'header', text = 'Player Settings:', align = 'center'},
			{type = 'spinner', text = 'Light of the Protector', key = 'protector', default = 75, desc = 'At what % health do we use Light of the Protector.'},
			{type = 'spinner', text = 'Guardian of Ancient Kings', key = 'goak', default = 40, desc = 'At what % health do we use Guardian of Ancient Kings.'},
			{type = 'spinner', text = 'Ardent Defender', key = 'ardent', default = 25, desc = 'At what % health do we use Ardent Defender.'},
			{type = 'spinner', text = 'Lay on Hands', key = 'loh', default = 10, desc = 'At what % health do we use Lay on Hands.'},
	}
}

NeP.Interface.buildGUI(config)

local init = function()
	NeP.Interface.CreateSetting('Class Settings', function() NeP.Interface.ShowGUI('ProtPallyRotation') end)
end

local target = {
	{{ -- Targeting
		{ '/targetenemy [noexists]', { -- Target an enemy
			'!target.exists',
		}},


	}, (function() return fetchKey('ProtPallyRotation', 'Target') end)},
}

local keybinds = {

}

local shared = {

	
	-- WoD Rune
	{ '#128482', '!player.buff(175439)'},
}

local raidEvents = {
	-- WoD 5 Mans
	{ '53600', {
		'target.casting(Pierce Armor)',
		'target.threat == 100'
	}},

	-- HM
	
	-- BRF
	
	
	-- HFC
	-- Iron reaver
	{ '498', {
		'player.debuff(Artillery)'
	}},
	
	-- Kilrogg
	{ '53600', {
		'target.casting(180199)',
		'target.threat == 100'
	}},
	
	-- Soc
	{ '53600', { -- Reverberating Blow
		'target.casting(Reverberating Blow)',
		'target.threat == 100'
	}},
	
	-- Fellord
	{ '53600', {
		'target.casting(179406)',
		'target.threat == 100'
	}},
	
	-- Xhul
	{ '53600', { -- Fel Strike
		'target.casting(Fel Strike)',
		'target.threat == 100'
	}},
	{ '53600', { -- Void Strike
		'target.casting(Void Strike)',
		'target.threat == 100'
	}},
	
	-- Manno
	{ '53600', { -- Glaive Thrust
		'target.casting(Glaive Thrust)',
		'target.threat == 100'
	}},
	{ '53600', { -- Massive Blast
		'target.casting(Massive Blast)',
		'target.threat == 100'
	}},
}

local interrupts = {
	{ '96231', nil, 'target'}, -- Rebuke
	
	{ '31935', nil, 'target'}, -- Avenging Shield
	
	{ '853', '!target.boss', 'target'}, -- Hammer of Justice
}

local survival = {
	{ '184092', { -- Light of the Protector
		(function() return dynEval('player.health <= '..fetchKey('ProtPallyRotation', 'protector')) end),
		'player.buff(188370)'
	}},
	-- SotR
	{ "53600", {
		'player.spell.charges > 1',
		'!player.buff',
		"target.range <= 8",
		'target.threat == 100'
	}},

}

local cooldowns = {
	{ '/use Sanctus, Sigil of the Unbroken', {
		'player.buff(31884)',
		'!player.buff(187617)',
		'target.range <= 8',
		(function() return fetchKey('ProtPallyRotation', 'ring') end)
	}},
	
	{ '31884', { -- Avenging Wrath
		'target.range <= 8'
	}},

	-- Survival CDs
	{ "Lay on Hands", {
		(function() return dynEval('player.health <= '..fetchKey('ProtPallyRotation', 'loh')) end),
		"!player.debuff(Forbearance)"}
	},
	{ "31850", { -- Ardent Defender
		(function() return dynEval('player.health <= '..fetchKey('ProtPallyRotation', 'ardent')) end),
		"modifier.cooldowns", }
	},
	{ "86659", { -- BoAK
		(function() return dynEval('player.health <= '..fetchKey('ProtPallyRotation', 'goak')) end),
		"modifier.cooldowns",}
	},
	{ "20594", { -- Stoneform
		"!player.buff(20594)",
		"player.health <= 75",
		"player.spell(20594).cooldown = 0" }
	},
	{ "#109223", { -- Healing Tonic
		"player.health <= 50",
	}},
	{ "#5512", { -- Healthstone
		"player.health <= 50" 
	}}, 
	
	{ "#trinket1", "player.health <= 65"},
	{ "#trinket2", "player.health <= 65"},
}

local rotation = {
	{{ -- Multitarget
		{ '31935'}, -- Avengers Shield
		{ "26573", { -- Consecration
			"target.range <= 8",
			"target.enemy",
			'!player.buff',
			'!player.moving'
		}},
		{ '204019', { -- Blessed Hammer
			'target.range <= 8',
			'target.enemy',
			'player.lastcast(20271)'
		}},
		{ "20271", { -- Judgement
			'target.range <= 30'
		}},
	}, 'player.area(8).enemies >= 2'},

	{{ -- Single Target
		{ "20271", { -- Judgement
			'target.range <= 30'
		}},
		{ '204019', { -- Blessed Hammer
			'target.range <= 8',
			'target.enemy',
			'player.lastcast(20271)',
			'target.debuff(204301)'
		}},
		{ "26573", { -- Consecration
			"target.range <= 8",
			"target.enemy",
			'!player.buff',
			'!player.moving'
		}},
		{ '204019', { -- Blessed Hammer
			'target.range <= 8',
			'target.enemy',
			'player.lastcast(204019)',
			'target.debuff(204301)'
		}},
		{ '31935'}, -- Avengers Shield
		{ '204019', { -- Blessed Hammer
			'target.range <= 8',
			'target.enemy'
		}},
	}, },
}

NeP.Engine.registerRotation(66, 'Prot Paladin - 7.03 (WoD)', 
{ -- Incombat
	{ target},
	{ keybinds},
	{ shared},
	{ raidEvents, 'modifier.raid'},
	{ survival},
	{ cooldowns, 'modifier.cooldowns'},
	{ interrupts, 'target.interruptAt(45)'},
	{ rotation},
},{ -- OOC
	{ target},
	{ raidEvents},
	{ keybinds},
	{ shared, '!player.mounted'},
}, 

init)