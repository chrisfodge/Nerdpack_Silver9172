local shared = {
	{ '20217', { -- Kings
		'!player.buff(20217).any',
		'!player.buff(1126).any',
		'!player.buff(115921).any',
		'!player.buff(116781).any'
	}},

	-- Might
	{ '19740', {
		'player.buff(20217).any',
		'!player.buff(20217)',
		'!player.buff(19740).any',
		'!player.buff(24907).any'
	}},
	{ '19740', {
		'player.buff(1126).any',
		'!player.buff(20217)',
		'!player.buff(19740).any',
		'!player.buff(24907).any'
	}},
	{ '19740', {
		'player.buff(115921).any',
		'!player.buff(20217)',
		'!player.buff(19740).any',
		'!player.buff(24907).any'
	}},
	{ '19740', {
		'player.buff(116781).any',
		'!player.buff(20217)',
		'!player.buff(19740).any',
		'!player.buff(24907).any'
	}},

	-- Seal
	--{ '20154', {
	--	'player.seal != 1',
	--	'!talent(7,1)'
	--}},

	-- Sacred Shield
	{ '20925', '!player.buff', 'player'},
}

local interrupts = {
	{ '96231'}, -- Rebuke

	{ '105593', '!target.boss'}, -- Fist of Justice
}

local survival = {
	{ "59752", "player.state.charm" },
	{ "59752", "player.state.fear" },
	{ "59752", "player.state.incapacitate" },
	{ "59752", "player.state.sleep" },
	{ "59752", "player.state.stun" },
}

local cooldowns = {
	{ '31884', '!player.buff'},
}

local rotation = {
	{ '53385',{
		'player.buff(157048)',
		'player.buff(144595)'
	}},
	{ "157048", { -- Final Verdict
		"player.holypower = 5",
		"!modifier.multitarget"
	}},
	{ "53385", { -- Divine Storm
		"player.holypower = 5",
		"modifier.multitarget"
	}},
	{ "114157", nil, 'target'}, -- Execution Sentence
	{ '158392'}, -- Hammer of Wrath
	{ "35395", "!modifier.multitarget"},
	{ "53595", "modifier.multitarget"},
	{ "20271"},
	{ "879"},
	{ "157048", {
	  "player.holypower >= 3",
	  "!modifier.multitarget"}
	},
	{ '20925'},
}

NeP.Engine.registerRotation(70, 'Ret Paladin v 0.8', {
	{ shared},
	{ survival},
	{ cooldowns},
	{ interrupts, 'target.interruptAt(45)'},
	{ rotation},
},

{ -- Out of Combat
	{ shared, '!player.mounted'}
}
)


