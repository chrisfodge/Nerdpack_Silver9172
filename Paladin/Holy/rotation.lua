-- ProbablyEngine Rotation Packager
-- Custom Holy Pally
local dynEval = NePCR.dynEval
local fetchKey = NeP.Interface.fetchKey

local config = {
	key = 'HolyPaladinRotation',
	profiles = true,
	title = 'Config',
	subtitle = 'Holy Paladin Settings',
	color = NeP.Core.classColor('player'),
	width = 200,
	height = 500,
	config = {
		-- General Settings
		{type = 'header', text = 'General Settings:', align = 'center'},
			{type = 'checkbox', text = 'Target', key = 'Target', default = true, desc = 'Auto target new target when old one dies.'},
			{type = 'checkbox', text = 'WoD Ring', key = 'ring', default = true, desc = 'Auto use ring as DPS CD.'},
			{type = 'dropdown',
				text = 'Buff', key = 'Buff', 
				list = {
			    	{text = 'Kings',key = 'kings'},
			        {text = 'Might',key = 'might'},
		    	}, 
		    	default = 'kings', 
		    	desc = 'Select which buff to use.' 
			},
			
			{type = 'spacer'},{type = 'rule'},
		{type = 'header', text = 'Player Settings:', align = 'center'},	
	}
}

NeP.Interface.buildGUI(config)

local init = function()
	NeP.Interface.CreateSetting('Class Settings', function() NeP.Interface.ShowGUI('HolyPaladinRotation') end)
end

local target = {
	{{ -- Targeting
		{ '/targetenemy [noexists]', { -- Target an enemy
			'!target.exists',
		}},
	}, (function() return fetchKey('HolyPaladinRotation', 'Target') end)},
}

local shared = {
	{{ -- Buff Management
		{'20217', {
			'!lowest.buffs.stats',
			(function() return fetchKey('HolyPaladiRotation', 'Buff') == 'kings' end),
		}},
		{'19740', {
			'!lowest.buffs.mastery',
			(function() return fetchKey('HolyPaladinRotation', 'Buff') == 'might' end),
		}},
	}},
}

local dispel = {

}

local cooldowns = {

}

local keybinds = {

}

local tankHealing = {

}

local AoE = {

}

local emergencyHealing = {

}

local moving = {

}

local lowMana = {

}

local raidHealing = {

}

local damage = {
	{ '20473'}, -- Holy Shock
	{ '20271'}, -- judge
	{ '35395', 'target.range <= 8'}, -- CS
	{ '2812'}, -- Denounce
}

NeP.Engine.registerRotation(65, 'Holy Paladin - Silver9172', {
	{ shared},
	{ target},
	
	{ damage},

},

{ -- Out of Combat
	{ target},
	{ shared},
	{ keybinds},

},

init)


